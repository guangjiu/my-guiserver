package NewChatRoom;
//用于储存用户信息
public class UserInfo {
    private String name;
    private String password;
    public UserInfo(String name,String password){
        this.name = name;
        this.password = password;
    }
    public UserInfo(){}
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getPsw(){
        return this.password;
    }
    public void setPassword(String password){
        this.password = password;
    }
}
