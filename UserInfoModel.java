package NewChatRoom;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;

public class UserInfoModel implements TableModel {
    List<serverHandlerThread> sht = ChatThreadManager.getAllServThread();
    public UserInfoModel(List<serverHandlerThread> sht){
        this.sht = sht;
    }
    @Override
    public int getRowCount() {
        return sht.size();
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return sht.get(rowIndex).getOwnUser().getName();
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {

    }
}
