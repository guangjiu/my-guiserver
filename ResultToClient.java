package NewChatRoom;

import javax.swing.*;

public class ResultToClient {
    public void print(String str){
        JTextArea ta = ClientGUI.ta_result;
        ta.append(str);
        //有新信息时滚动最底
        ta.setCaretPosition(ta.getDocument().getLength());
    }
}
