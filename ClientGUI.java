package NewChatRoom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientGUI {
    private JFrame jf;
    private ChatClient chatclient;
    public static JTextArea ta_result;
    private JTextField tf_ip;
    private JTextField tf_port;
    public static JButton b_connect;
    private JTextArea ta_send;
    private ClientSendMsg sendTool;
    ResultToClient rs = new ResultToClient();
    public static void main(String[] args){

        new ClientGUI().createGUI();
    }
    public void createGUI(){
        jf = new JFrame("聊天室客户端:未连接");
        jf.setLocationRelativeTo(null);
        jf.setSize(600,600);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setLayout(new FlowLayout());
        JLabel l_ip = new JLabel("ip地址:");
        jf.add(l_ip);
        tf_ip = new JTextField(15);
        tf_ip.setText("127.0.0.1");
        jf.add(tf_ip);
        JLabel l_port = new JLabel("端口号：");
        jf.add(l_port);
        tf_port = new JTextField(6);
        tf_port.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                int keyChar = e.getKeyChar();
                //屏蔽非法输入
                if(keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9){

                }else {
                    e.consume();
                }
            }
        });
        jf.add(tf_port);

        b_connect = new JButton("连接");
        b_connect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startClient();
            }
        });
        b_connect.setPreferredSize(new Dimension(80, 30));
        jf.add(b_connect);

        ta_result = new JTextArea("返回的消息\n");
        ta_result.setEditable(false);
        JScrollPane result_area = new JScrollPane(ta_result);
        result_area.setPreferredSize(new Dimension(400, 100));
        jf.add(result_area);

        ta_send = new JTextArea();
        JScrollPane send_area = new JScrollPane(ta_send);
        send_area.setPreferredSize(new Dimension(400, 100));
        jf.add(send_area);

        JButton b_send = new JButton("发送");
        b_send.setPreferredSize(new Dimension(80, 30));
        jf.add(b_send);
        b_send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMsg();
            }
        });

        jf.setVisible(true);
    }

    private void sendMsg(){
        String msg = ta_send.getText();
        if(sendTool!=null)
            sendTool.sendMsg(msg);
        ta_send.setText("");
    }

    private void startClient(){
        if(chatclient==null){
            String ip =tf_ip.getText();
            int port = Integer.parseInt(tf_port.getText());
            Socket socket;
            try {
                socket = new Socket(ip,port);
            }catch (IOException e){
                e.printStackTrace();
                rs.print("连接服务器失败\n");
                return;
            }
            chatclient = new ChatClient(socket);
            chatclient.start();
            sendTool = new ClientSendMsg(chatclient.getSocket()).init();
            jf.setTitle("聊天室客户端:已连接");
            b_connect.setText("断开");
        }else if(chatclient.isRunning()){
            chatclient.stopChatClient();
            chatclient = null;
            sendTool = null;
            rs.print("已断开连接\n");
            jf.setTitle("聊天室客户端:未连接");
            b_connect.setText("连接");
        }
    }

}
