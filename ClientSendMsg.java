package NewChatRoom;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientSendMsg {
    private Socket socket;
    private DataOutputStream dos;
    public ClientSendMsg(Socket socket){this.socket=socket;}
    public ClientSendMsg init(){
        try {
            dos = new DataOutputStream(socket.getOutputStream());
        }catch (IOException e){
            e.printStackTrace();
        }
        return this;
    }
    public void sendMsg(String str){
        try {
            dos.writeUTF(str);
            dos.flush();
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
